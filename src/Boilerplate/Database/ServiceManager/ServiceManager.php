<?php

/**
 * Boilerplate Module
 *
 * @author Jason Brown <jason.brown@jbfreelance.co.uk>
 */

namespace Boilerplate\Database\ServiceManager;

use Boilerplate\Database\DatabaseServiceInterface;
use Boilerplate\Database\ServiceManager\Exception\NoDatabaseServiceForKeyException;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class ServiceManager
 * @package Boilerplate\Database
 */
class ServiceManager
{
    /**
     * @var ArrayCollection
     */
    protected $databaseServices;

    /**
     * Constructor
     *
     * @param array $databaseServices
     */
    public function __construct(array $databaseServices = array())
    {
        $this->setDatabaseServices($databaseServices);
    }

    /**
     * Get all Database Services associated with the Manager
     *
     * @return ArrayCollection
     */
    public function getDatabaseServices()
    {
        if(!$this->databaseServices instanceof ArrayCollection)
        {
            $this->databaseServices = new ArrayCollection();
        }

        return $this->databaseServices;
    }

    /**
     * Sets Database Service Collection
     *
     * @param array $databaseServices
     * @return $this
     */
    public function setDatabaseServices(array $databaseServices = array())
    {
        foreach($databaseServices as $key => $service)
        {
            if($service instanceof DatabaseServiceInterface)
                $this->addDatabaseService($key, $service);
        }

        return $this;
    }

    /**
     * Add a Database Service to the collection
     *
     * @param $key
     * @param DatabaseServiceInterface $databaseService
     * @throws \Exception
     */
    public function addDatabaseService($key, DatabaseServiceInterface $databaseService)
    {
        $this->getDatabaseServices()->set($key, $databaseService);
    }

    /**
     * Gets a Database Service by the given key
     *
     * @param $key
     * @return DatabaseServiceInterface
     * @throws NoDatabaseServiceForKeyException
     */
    public function get($key)
    {
        if($this->getDatabaseServices()->containsKey($key))
        {
            return $this->getDatabaseServices()->get($key);
        }else{
            throw new NoDatabaseServiceForKeyException('No Database Service for key: ' . $key);
        }
    }

    /**
     * Removes a Database Service by the given key
     *
     * @param $key
     * @return mixed|null
     * @throws NoDatabaseServiceForKeyException
     */
    public function remove($key)
    {
        if($this->getDatabaseServices()->containsKey($key))
        {
            return $this->getDatabaseServices()->remove($key);
        }else{
            throw new NoDatabaseServiceForKeyException('No Database Service for key: ' . $key);
        }
    }
}