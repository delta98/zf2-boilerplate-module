<?php

/**
 * Boilerplate Module
 *
 * @author Jason Brown <jason.brown@jbfreelance.co.uk>
 */

namespace Boilerplate\Database\ServiceManager\Exception;

use Boilerplate\Exception as BoilerplateException;

/**
 * Class NoDatabaseServiceForKeyException
 * @package Boilerplate\Database\ServiceManager
 */
class NoDatabaseServiceForKeyException extends BoilerplateException {}