<?php

/**
 * Boilerplate Module
 *
 * @author Jason Brown <jason.brown@jbfreelance.co.uk>
 */

namespace Boilerplate\Database;

/**
 * Class DatabaseServiceAwareInterface
 * @package Boilerplate\Database
 */
interface DatabaseServiceAwareInterface
{
    /**
     * Get the Database Service
     *
     * @return DatabaseServiceInterface
     */
    public function getDatabaseService();

    /**
     * Set Database Service
     *
     * @param DatabaseServiceInterface $databaseService
     */
    public function setDatabaseService(DatabaseServiceInterface $databaseService);
}