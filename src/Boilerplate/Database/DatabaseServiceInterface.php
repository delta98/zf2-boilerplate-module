<?php

/**
 * Boilerplate Module
 *
 * @author Jason Brown <jason.brown@jbfreelance.co.uk>
 */

namespace Boilerplate\Database;

use Boilerplate\Database\DatabaseObjectInterface;

/**
 * Class DatabaseInterface
 * @package Boilerplate\Database
 */
interface DatabaseServiceInterface
{
    /**
     * Insert Database Object
     *
     * @param DatabaseObjectInterface $object
     * @return DatabaseObjectInterface|boolean
     */
    public function insert(DatabaseObjectInterface $object);

    /**
     * Update Database Object
     *
     * @param DatabaseObjectInterface $object
     * @return DatabaseObjectInterface|boolean
     */
    public function update(DatabaseObjectInterface $object);

    /**
     * Delete Database Object
     *
     * @param DatabaseObjectInterface $object
     * @return DatabaseObjectInterface|boolean
     */
    public function delete(DatabaseObjectInterface $object);

    /**
     * Find an Object by it's id
     *
     * @param string $className
     * @param int $id
     * @return DatabaseObjectInterface
     */
    public function find($className, $id);

    /**
     * Find all Objects
     *
     * @param $className
     * @return \IteratorAggregate
     */
    public function findAll($className);

    /**
     * Find an Object by a given criteria
     *
     * @param string $className
     * @param mixed $criteria
     * @param mixed $orderBy
     * @param int | null $limit
     * @param int | null $offset
     * @return DatabaseObjectInterface
     */
    public function findBy($className, $criteria = array(), $orderBy = array(), $limit = null, $offset = null);

    /**
     * Find a single Object by a given criteria
     *
     * @param string $className
     * @param mixed $criteria
     * @param mixed $orderBy
     * @return DatabaseObjectInterface
     */
    public function findOneBy($className, $criteria = array(), $orderBy = array());

    /**
     * Count the number of Objects available
     *
     * @param $className
     * @return int
     */
    public function count($className);
}

