<?php

/**
 * Boilerplate Module
 *
 * @author Jason Brown <jason.brown@jbfreelance.co.uk>
 */

namespace Boilerplate\Database;

/**
 * Class DatabaseServiceAwareTrait
 * @package Boilerplate\Database
 */
trait DatabaseServiceAwareTrait
{
    /**
     * @var DatabaseServiceInterface
     */
    protected $databaseService;

    /**
     * Get the Database Service
     *
     * @return DatabaseServiceInterface
     */
    public function getDatabaseService()
    {
        return $this->databaseService;
    }

    /**
     * Set Database Service
     *
     * @param DatabaseServiceInterface $databaseService
     */
    public function setDatabaseService(DatabaseServiceInterface $databaseService)
    {
        $this->databaseService = $databaseService;
    }
}