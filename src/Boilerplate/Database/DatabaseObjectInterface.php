<?php

/**
 * Boilerplate Module
 *
 * @author Jason Brown <jason.brown@jbfreelance.co.uk>
 */

namespace Boilerplate\Database;

/**
 * Class DatabaseObjectInterface
 * @package Boilerplate\Database
 */
interface DatabaseObjectInterface
{
    /**
     * Get Object id
     *
     * @return int
     */
    public function getId();

    /**
     * Convert Object to an array
     *
     * @return array
     */
    public function getArrayCopy();
}
