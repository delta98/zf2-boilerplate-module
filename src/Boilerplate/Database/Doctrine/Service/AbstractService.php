<?php

/**
 * Boilerplate Module
 *
 * @author Jason Brown <jason.brown@jbfreelance.co.uk>
 */

namespace Boilerplate\Database\Doctrine\Service;

use Doctrine\Common\Persistence\ObjectManager;
use Boilerplate\Database\DatabaseServiceInterface;
use Boilerplate\Database\DatabaseObjectInterface;
use Boilerplate\Log;
use Doctrine\DBAL\DBALException;
use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class AbstractService
 * @package Boilerplate\Database\Doctrine\Service
 */
abstract class AbstractService implements DatabaseServiceInterface, ServiceLocatorAwareInterface, ObjectManagerAwareInterface
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var string
     */
    protected $objectManagerServiceName;

    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    /**
     * Insert Database Object
     *
     * @param DatabaseObjectInterface $object
     * @return DatabaseObjectInterface|boolean
     */
    public function insert(DatabaseObjectInterface $object)
    {
        try{
            $this->getObjectManager()->persist($object);
            $this->getObjectManager()->flush();

            return $object;
        }catch(DBALException $e){
            // Log error
            Log::error($e->getMessage());
        }

        return false;
    }

    /**
     * Update Database Object
     *
     * @param DatabaseObjectInterface $object
     * @return DatabaseObjectInterface|boolean
     */
    public function update(DatabaseObjectInterface $object)
    {
        try{
            $this->getObjectManager()->merge($object);
            $this->getObjectManager()->flush();

            return $object;
        }catch(DBALException $e){
            // Log error
            Log::error($e->getMessage());
        }

        return false;
    }

    /**
     * Delete Database Object
     *
     * @param DatabaseObjectInterface $object
     * @return DatabaseObjectInterface|boolean
     */
    public function delete(DatabaseObjectInterface $object)
    {
        try{
            $this->getObjectManager()->remove($object);
            $this->getObjectManager()->flush();

            return true;
        }catch(DBALException $e){
            // Log error
            Log::error($e->getMessage());
        }

        return false;
    }

    /**
     * Find an Object by it's id
     *
     * @param string $className
     * @param int $id
     * @return DatabaseObjectInterface
     */
    public function find($className, $id)
    {
        return $this->getObjectManager()
                ->getRepository($className)
                ->find($id);
    }

    /**
     * Find all Objects
     *
     * @param $className
     * @return \IteratorAggregate
     */
    public function findAll($className)
    {
        return $this->getObjectManager()
            ->getRepository($className)
            ->findAll();
    }

    /**
     * Find an Object by a given criteria
     *
     * @param string $className
     * @param mixed $criteria
     * @param mixed $orderBy
     * @param int | null $limit
     * @param int | null $offset
     * @return DatabaseObjectInterface
     */
    public function findBy($className, $criteria = array(), $orderBy = array(), $limit = null, $offset = null)
    {
       return $this->getObjectManager()
                    ->getRepository($className)
                    ->findBy($criteria, $orderBy, $limit, $offset);
    }

    /**
     * Find a single Object by a given criteria
     *
     * @param string $className
     * @param mixed $criteria
     * @param mixed $orderBy
     * @return DatabaseObjectInterface
     */
    public function findOneBy($className, $criteria = array(), $orderBy = array())
    {
        return $this->getObjectManager()
                    ->getRepository($className)
                    ->findOneBy($criteria, $orderBy);
    }

    /**
     * Count the number of Objects available
     *
     * @param $className
     * @return int
     */
    public function count($className)
    {
        $qb = $this->getObjectManager()->createQueryBuilder();
        $query = $qb->select('COUNT(o.id)')
            ->from($className, 'o')
            ->getQuery();

        return $query->getSingleScalarResult();
    }

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    /**
     * Set the Object Manager
     *
     * @param ObjectManager $objectManager
     */
    public function setObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * Return a Doctrine ObjectManager
     *
     * @return ObjectManager
     */
    public function getObjectManager()
    {
        // Check if Object Manager is open
        // If it's closed, usually this means there has been an error
        if($this->objectManager === null || (method_exists($this->objectManager, 'isOpen') && !$this->objectManager->isOpen()))
        {
            // We want to recover and create a new instance of the Document manager
            $this->setObjectManager($this->getObjectManagerFromFactory());

            // But we should also create an error in the log
            Log::error('Object Manager was closed. An exception occurred.');
        }

        return $this->objectManager;
    }

    /**
     * Set the Object Manager Service name
     *
     * @param string $serviceName
     */
    public function setObjectManagerServiceName($serviceName)
    {
        $this->objectManagerServiceName = $serviceName;
    }

    /**
     * Get the Object Manager Service name
     *
     * @return string
     */
    public function getObjectManagerServiceName()
    {
        return $this->objectManagerServiceName;
    }

    /**
     * Get Object Manager from Factory
     *
     * @return ObjectManager
     */
    protected abstract function getObjectManagerFromFactory();
}

