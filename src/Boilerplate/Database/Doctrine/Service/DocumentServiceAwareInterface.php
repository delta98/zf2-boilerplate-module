<?php

/**
 * Boilerplate Module
 *
 * @author Jason Brown <jason.brown@jbfreelance.co.uk>
 */

namespace Boilerplate\Database\Doctrine\Service;

/**
 * Class EntityServiceAwareInterface
 * @package Boilerplate\Database\Doctrine\Service
 */
interface DocumentServiceAwareInterface
{
    /**
     * Set the Document Service
     *
     * @param Document $entityService
     * @return mixed
     */
    public function setDocumentService(Document $entityService);

    /**
     * Get the Document Service
     *
     * @return Document
     */
    public function getDocumentService();
}