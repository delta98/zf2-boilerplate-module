<?php

/**
 * Boilerplate Module
 *
 * @author Jason Brown <jason.brown@jbfreelance.co.uk>
 */

namespace Boilerplate\Database\Doctrine\Service;

use Doctrine\ODM\MongoDB\DocumentManager;
use DoctrineMongoODMModule\Service\DocumentManagerFactory;

/**
 * Class Document
 * @package Boilerplate\Database\Doctrine\Service
 */
class Document extends AbstractService
{
    /**
     * @var string
     */
    protected $objectManagerServiceName = 'odm_default';

    /**
     * Get Object Manager from Factory
     *
     * @return DocumentManager
     */
    protected function getObjectManagerFromFactory()
    {
        $em = new DocumentManagerFactory($this->getObjectManagerServiceName());
        return $em->createService($this->getServiceLocator());
    }

    /**
     * Count the number of Objects available
     *
     * @param $className
     * @return int
     */
    public function count($className)
    {
        return $this->getObjectManager()->getDocumentCollection($className)->count();
    }
}