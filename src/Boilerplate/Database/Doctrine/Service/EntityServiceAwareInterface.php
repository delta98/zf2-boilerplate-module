<?php

/**
 * Boilerplate Module
 *
 * @author Jason Brown <jason.brown@jbfreelance.co.uk>
 */

namespace Boilerplate\Database\Doctrine\Service;

/**
 * Class EntityServiceAwareInterface
 * @package Boilerplate\Database\Doctrine\Service
 */
interface EntityServiceAwareInterface
{
    /**
     * Set the Entity Service
     *
     * @param Entity $entityService
     * @return mixed
     */
    public function setEntityService(Entity $entityService);

    /**
     * Get the Entity Service
     *
     * @return Entity
     */
    public function getEntityService();
}