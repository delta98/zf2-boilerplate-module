<?php

/**
 * Boilerplate Module
 *
 * @author Jason Brown <jason.brown@jbfreelance.co.uk>
 */

namespace Boilerplate\Database\Doctrine\Service;

use Doctrine\ORM\EntityManager;
use DoctrineORMModule\Service\EntityManagerFactory;

/**
 * Class Entity
 * @package Boilerplate\Database\Doctrine\Service
 */
class Entity extends AbstractService
{
    /**
     * @var string
     */
    protected $objectManagerServiceName = 'orm_default';

    /**
     * Get Object Manager from Factory
     *
     * @return EntityManager
     */
    protected function getObjectManagerFromFactory()
    {
        $em = new EntityManagerFactory($this->getObjectManagerServiceName());
        return $em->createService($this->getServiceLocator());
    }
}