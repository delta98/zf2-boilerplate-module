<?php

/**
 * Boilerplate Module
 *
 * @author Jason Brown <jason.brown@jbfreelance.co.uk>
 */

namespace Boilerplate\Database\Doctrine\Entity;

use Boilerplate\Database\DatabaseObjectInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class BaseEntity
 * @package Boilerplate\Database\Doctrine\Entity
 * @ORM\MappedSuperclass
 */
class BaseEntity implements DatabaseObjectInterface
{
    /** @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue **/
    protected $id;

    /**
     * Get Object id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Convert Object to an array
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}