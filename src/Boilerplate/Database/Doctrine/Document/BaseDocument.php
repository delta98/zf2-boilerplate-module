<?php

/**
 * Boilerplate Module
 *
 * @author Jason Brown <jason.brown@jbfreelance.co.uk>
 */

namespace Boilerplate\Database\Doctrine\Document;

use Boilerplate\Database\DatabaseObjectInterface;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Class BaseDocument
 * @package Boilerplate\Database\Doctrine\Document
 * @ODM\MappedSuperclass
 */
class BaseDocument implements DatabaseObjectInterface
{
    /**
     * @ODM\Id
     */
    protected $id;

    /**
     * Get Object id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Convert Object to an array
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}