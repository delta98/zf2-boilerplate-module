<?php

/**
 * Boilerplate Module
 *
 * @author Jason Brown <jason.brown@jbfreelance.co.uk>
 */

namespace Boilerplate;

/**
 * Class Exception
 * @package Boilerplate
 */
class Exception extends \Exception
{
    /**
     * Construct the exception and log message to error log
     *
     * @param string $message [optional] The Exception message to throw.
     * @param int $code [optional] The Exception code.
     * @param Exception $previous [optional] The previous exception used for the exception chaining. Since 5.3.0
     */
    public function __construct($message = "", $code = 0, Exception $previous = null)
    {
        // Log exception message to error log
        Log::error($message);

        // Continue constructing Exception
        parent::__construct($message, $code, $previous);
    }
}

?>
