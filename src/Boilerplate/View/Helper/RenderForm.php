<?php

/**
 * Boilerplate Module
 *
 * @author Jason Brown <jason.brown@jbfreelance.co.uk>
 */

namespace Boilerplate\View\Helper;

use Zend\Form\Form;
use Zend\View\Helper\AbstractHelper;

/**
 * Class RenderForm
 * @package Boilerplate\View\Helper
 */
class RenderForm extends AbstractHelper
{
    /**
     * Renders a Form with it's elements
     *
     * @param Form $form
     * @return string
     */
    public function __invoke(Form $form)
    {
        $form->prepare();
        $elements = $form->getElements();
        
        $output = $this->view->form()->openTag($form).PHP_EOL;
        
        foreach($elements as $element)
        {
            $output .= $this->view->formRow($element).PHP_EOL;
        }
        
        $output .= $this->view->form()->closeTag($form).PHP_EOL;
        
        return $output;
    }
}

?>
