<?php

/**
 * Boilerplate Module
 *
 * @author Jason Brown <jason.brown@jbfreelance.co.uk>
 */

namespace Boilerplate;

use Zend\Log\Writer\Stream;
use Zend\Log\Logger;

/**
 * Class Log
 * @package Boilerplate
 */
class Log
{
    /**
     * Logs an Error Message
     *
     * @param $message
     */
    public static function error($message)
    {
        $writer = new Stream(__DIR__.'/../../../../../data/logs/error.log');
        $logger = new Logger();
        $logger->addWriter($writer);

        $logger->err($message);
    }

    /**
     * Logs a Message
     *
     * @param $message
     */
    public static function info($message)
    {
        $writer = new Stream(__DIR__.'/../../../../../data/logs/info.log');
        $logger = new Logger();
        $logger->addWriter($writer);

        $logger->info($message);
    }
}
