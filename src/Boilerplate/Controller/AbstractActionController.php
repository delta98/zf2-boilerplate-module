<?php

/**
 * Boilerplate Module
 *
 * @author Jason Brown <jason.brown@jbfreelance.co.uk>
 */

namespace Boilerplate\Controller;

use Zend\Mvc\Controller\AbstractActionController as ZendAbstractActionController;

/**
 * Class AbstractActionController
 * @package Boilerplate\Controller
 */
class AbstractActionController extends ZendAbstractActionController
{
    /**
     * Get the Database Service from the Service Manager
     *
     * @param string $key
     * @return \Boilerplate\Database\DatabaseServiceInterface
     */
    public function getDatabaseService($key = 'default')
    {
        return $this->getServiceLocator()->get('Database\ServiceManager')->get($key);
    }
}