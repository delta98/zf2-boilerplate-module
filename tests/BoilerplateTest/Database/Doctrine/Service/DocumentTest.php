<?php

/**
 * Boilerplate Module
 *
 * @author Jason Brown <jason.brown@jbfreelance.co.uk>
 */

namespace BoilerplateTest\Database\Service;

use Boilerplate\Database\Doctrine\Service\Document as DocumentService;
use BootstrapTest\Bootstrap;

/**
 * Class DocumentTest
 * @package BoilerplateTest\Database\Service
 */
class DocumentTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var DocumentService
     */
    protected $documentService;

    public function setUp()
    {
        $this->documentService = new DocumentService();
        $this->documentService->setServiceLocator(Bootstrap::getServiceManager());
    }

    public function tearDown()
    {
        $this->documentService = null;
    }

    public function testGetObjectManagerServiceName()
    {
        $this->assertEquals('odm_default', $this->documentService->getObjectManagerServiceName());
    }

    public function testDocumentManagerReturnedFromFactory()
    {
        $this->assertInstanceOf('Doctrine\ODM\MongoDB\DocumentManager', $this->documentService->getObjectManager());
    }
}