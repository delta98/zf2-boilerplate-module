<?php

/**
 * Boilerplate Module
 *
 * @author Jason Brown <jason.brown@jbfreelance.co.uk>
 */

namespace BoilerplateTest\Database\Service;
use Doctrine\DBAL\DBALException;

/**
 * Class AbstractServiceTest
 * @package BoilerplateTest\Database\Service
 */
class AbstractServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Boilerplate\Database\Doctrine\Service\AbstractService
     */
    protected $abstractService;

    public function setUp()
    {
        $this->abstractService = $this->getMockForAbstractClass('Boilerplate\Database\Doctrine\Service\AbstractService');
    }

    public function tearDown()
    {
        $this->abstractService = null;
    }

    public function testInsertDatabaseObjectInterface()
    {
        $objectManager = $this->getMock('Doctrine\Common\Persistence\ObjectManager');

        $this->abstractService->expects($this->any())
             ->method('getObjectManagerFromFactory')
             ->will($this->returnValue($objectManager));

        $databaseObject = $this->getMock('Boilerplate\Database\DatabaseObjectInterface');
        $this->assertInstanceOf('Boilerplate\Database\DatabaseObjectInterface', $this->abstractService->insert($databaseObject));
    }

    public function testInsertDatabaseObjectInterfaceFailed()
    {
        $objectManager = $this->getMock('Doctrine\Common\Persistence\ObjectManager');
        $objectManager->expects($this->any())
                      ->method('flush')
                      ->will($this->throwException(new DBALException()));


        $this->abstractService->expects($this->any())
              ->method('getObjectManagerFromFactory')
              ->will($this->returnValue($objectManager));

        $databaseObject = $this->getMock('Boilerplate\Database\DatabaseObjectInterface');
        $this->assertFalse($this->abstractService->insert($databaseObject));
    }

    public function testUpdateDatabaseObjectInterface()
    {
        $objectManager = $this->getMock('Doctrine\Common\Persistence\ObjectManager');

        $this->abstractService->expects($this->any())
              ->method('getObjectManagerFromFactory')
              ->will($this->returnValue($objectManager));

        $databaseObject = $this->getMock('Boilerplate\Database\DatabaseObjectInterface');
        $this->assertInstanceOf('Boilerplate\Database\DatabaseObjectInterface', $this->abstractService->update($databaseObject));
    }

    public function testUpdateDatabaseObjectInterfaceFailed()
    {
        $objectManager = $this->getMock('Doctrine\Common\Persistence\ObjectManager');
        $objectManager->expects($this->any())
                      ->method('flush')
                      ->will($this->throwException(new DBALException()));


        $this->abstractService->expects($this->any())
              ->method('getObjectManagerFromFactory')
              ->will($this->returnValue($objectManager));

        $databaseObject = $this->getMock('Boilerplate\Database\DatabaseObjectInterface');
        $this->assertFalse($this->abstractService->update($databaseObject));
    }

    public function testDeleteDatabaseObjectInterface()
    {
        $objectManager = $this->getMock('Doctrine\Common\Persistence\ObjectManager');

        $this->abstractService->expects($this->any())
              ->method('getObjectManagerFromFactory')
              ->will($this->returnValue($objectManager));

        $databaseObject = $this->getMock('Boilerplate\Database\DatabaseObjectInterface');
        $this->assertTrue($this->abstractService->delete($databaseObject));
    }

    public function testDeleteDatabaseObjectInterfaceFailed()
    {
        $objectManager = $this->getMock('Doctrine\Common\Persistence\ObjectManager');
        $objectManager->expects($this->any())
                      ->method('flush')
                      ->will($this->throwException(new DBALException()));


        $this->abstractService->expects($this->any())
              ->method('getObjectManagerFromFactory')
              ->will($this->returnValue($objectManager));

        $databaseObject = $this->getMock('Boilerplate\Database\DatabaseObjectInterface');
        $this->assertFalse($this->abstractService->delete($databaseObject));
    }

    public function testFindDatabaseObjectInterface()
    {
        $objectManager    = $this->getMock('Doctrine\Common\Persistence\ObjectManager');
        $objectRepository = $this->getMock('Doctrine\Common\Persistence\ObjectRepository');
        $databaseObject   = $this->getMock('Boilerplate\Database\DatabaseObjectInterface');

        $objectRepository->expects($this->any())
                          ->method('find')
                          ->with($this->equalTo(1))
                          ->will($this->returnValue($databaseObject));

        $objectManager->expects($this->any())
                      ->method('getRepository')
                      ->with($this->equalTo('Boilerplate\Database\DatabaseObjectInterface'))
                      ->will($this->returnValue($objectRepository));

        $this->abstractService->expects($this->any())
              ->method('getObjectManagerFromFactory')
              ->will($this->returnValue($objectManager));

        $this->assertInstanceOf('Boilerplate\Database\DatabaseObjectInterface', $this->abstractService->find('Boilerplate\Database\DatabaseObjectInterface', 1));
    }

    public function testFindAllDatabaseObjectInterfaces()
    {
        $objectManager    = $this->getMock('Doctrine\Common\Persistence\ObjectManager');
        $objectRepository = $this->getMock('Doctrine\Common\Persistence\ObjectRepository');
        $databaseObject   = $this->getMock('Boilerplate\Database\DatabaseObjectInterface');

        $objectRepository->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue(array($databaseObject)));

        $objectManager->expects($this->any())
            ->method('getRepository')
            ->with($this->equalTo('Boilerplate\Database\DatabaseObjectInterface'))
            ->will($this->returnValue($objectRepository));

        $this->abstractService->expects($this->any())
            ->method('getObjectManagerFromFactory')
            ->will($this->returnValue($objectManager));

        $this->assertTrue(is_array($this->abstractService->findAll('Boilerplate\Database\DatabaseObjectInterface')));
    }

    public function testDatabaseObjectInterfaceFindBy()
    {
        $objectManager    = $this->getMock('Doctrine\Common\Persistence\ObjectManager');
        $objectRepository = $this->getMock('Doctrine\Common\Persistence\ObjectRepository');
        $databaseObject   = $this->getMock('Boilerplate\Database\DatabaseObjectInterface');

        $objectRepository->expects($this->any())
            ->method('findBy')
            ->will($this->returnValue(array($databaseObject)));

        $objectManager->expects($this->any())
            ->method('getRepository')
            ->with($this->equalTo('Boilerplate\Database\DatabaseObjectInterface'))
            ->will($this->returnValue($objectRepository));

        $this->abstractService->expects($this->any())
            ->method('getObjectManagerFromFactory')
            ->will($this->returnValue($objectManager));

        $this->assertTrue(is_array($this->abstractService->findBy('Boilerplate\Database\DatabaseObjectInterface')));
    }

    public function testDatabaseObjectInterfaceFindOneBy()
    {
        $objectManager    = $this->getMock('Doctrine\Common\Persistence\ObjectManager');
        $objectRepository = $this->getMock('Doctrine\Common\Persistence\ObjectRepository');
        $databaseObject   = $this->getMock('Boilerplate\Database\DatabaseObjectInterface');

        $objectRepository->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($databaseObject));

        $objectManager->expects($this->any())
            ->method('getRepository')
            ->with($this->equalTo('Boilerplate\Database\DatabaseObjectInterface'))
            ->will($this->returnValue($objectRepository));

        $this->abstractService->expects($this->any())
            ->method('getObjectManagerFromFactory')
            ->will($this->returnValue($objectManager));

        $this->assertInstanceOf('Boilerplate\Database\DatabaseObjectInterface', $this->abstractService->findOneBy('Boilerplate\Database\DatabaseObjectInterface'));
    }
}