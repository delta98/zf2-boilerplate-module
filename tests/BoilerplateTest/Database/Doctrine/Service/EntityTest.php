<?php

/**
 * Boilerplate Module
 *
 * @author Jason Brown <jason.brown@jbfreelance.co.uk>
 */

namespace BoilerplateTest\Database\Service;

use Boilerplate\Database\Doctrine\Service\Entity as EntityService;
use BootstrapTest\Bootstrap;

/**
 * Class Entity
 * @package BoilerplateTest\Database\Service
 */
class EntityTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var EntityService
     */
    protected $entityService;

    public function setUp()
    {
        $this->entityService = new EntityService();
        $this->entityService->setServiceLocator(Bootstrap::getServiceManager());
    }

    public function tearDown()
    {
        $this->entityService = null;
    }

    public function testGetObjectManagerServiceName()
    {
        $this->assertEquals('orm_default', $this->entityService->getObjectManagerServiceName());
    }

    public function testEntityManagerReturnedFromFactory()
    {
        $this->assertInstanceOf('Doctrine\ORM\EntityManager', $this->entityService->getObjectManager());
    }
}