<?php

/**
 * Boilerplate Module
 *
 * @author Jason Brown <jason.brown@jbfreelance.co.uk>
 */

namespace BoilerplateTest\Database\ServiceManager;

use Boilerplate\Database\ServiceManager\ServiceManager;

/**
 * Class ServiceManagerTest
 * @package BoilerplateTest
 */
class ServiceManagerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ServiceManager
     */
    protected $databaseServiceManager;

    public function setUp()
    {
        $this->databaseServiceManager = new ServiceManager();
    }

    public function tearDown()
    {
        $this->databaseServiceManager = null;
    }

    public function testDatabaseServiceCollectionIsEmpty()
    {
        $arrayCollection = $this->databaseServiceManager->getDatabaseServices();

        $this->assertEquals(0, $arrayCollection->count());
    }

    /*public function testAddingDatabaseServicesFromArray()
    {
        $mockInterface = $this->getMock('Boilerplate\Database\DatabaseServiceInterface');
        $services = array('default' => $mockInterface);
        $this->databaseServiceManager->setDatabaseServices($services);

        $arrayCollection = $this->databaseServiceManager->getDatabaseServices();

        $this->assertEquals(1, $arrayCollection->count());
    }*/

    public function testAddingDatabaseService()
    {
        $databaseService = $this->getMock('Boilerplate\Database\DatabaseServiceInterface');
        $this->databaseServiceManager->addDatabaseService('default', $databaseService);

        $arrayCollection = $this->databaseServiceManager->getDatabaseServices();

        $this->assertEquals(1, $arrayCollection->count());
    }

    public function testGettingDatabaseServiceWithException()
    {
        $this->setExpectedException('Boilerplate\Database\ServiceManager\Exception\NoDatabaseServiceForKeyException');
        $this->databaseServiceManager->get('default');
    }

    public function testAddingDatabaseServiceAndRetrievingFromManager()
    {
        $databaseService = $this->getMock('Boilerplate\Database\DatabaseServiceInterface');
        $this->databaseServiceManager->addDatabaseService('default', $databaseService);
        $databaseService = $this->databaseServiceManager->get('default');
        $this->assertInstanceOf('Boilerplate\Database\DatabaseServiceInterface', $databaseService);
    }

    public function testRemovingDatabaseService()
    {
        $databaseService = $this->getMock('Boilerplate\Database\DatabaseServiceInterface');
        $this->databaseServiceManager->addDatabaseService('default', $databaseService);
        $this->databaseServiceManager->remove('default');

        $arrayCollection = $this->databaseServiceManager->getDatabaseServices();

        $this->assertEquals(0, $arrayCollection->count());
    }

    public function testRemovingDatabaseServiceWithException()
    {
        $this->setExpectedException('Boilerplate\Database\ServiceManager\Exception\NoDatabaseServiceForKeyException');
        $this->databaseServiceManager->remove('default');
    }
}